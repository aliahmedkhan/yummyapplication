package com.example.yummyapplication.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class EmployeeData implements Parcelable {
    private String mName;
    private List<String> mLocations; //maybe make this object
    private String mTitle;

    public EmployeeData() {}

    protected EmployeeData(Parcel in) {
        mName = in.readString();
        mLocations = in.createStringArrayList();
        mTitle = in.readString();
    }

    public static final Creator<EmployeeData> CREATOR = new Creator<EmployeeData>() {
        @Override
        public EmployeeData createFromParcel(Parcel in) {
            return new EmployeeData(in);
        }

        @Override
        public EmployeeData[] newArray(int size) {
            return new EmployeeData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeStringList(mLocations);
        parcel.writeString(mTitle);
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public List<String> getLocations() {
        return mLocations;
    }

    public void setLocations(List<String> mLocations) {
        this.mLocations = mLocations;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}

package com.example.yummyapplication.Model;

/**
 * Represents current contract of json schema for employees
 */
public class EmployeeJsonConstants {
    public static String EMPLOYEES_ARRAY_KEY = "employees";
    public static String LOCATIONS_KEY = "locations";
    public static String NAME_KEY = "name";
    public static String TITLE_KEY = "title";
}

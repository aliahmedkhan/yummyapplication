package com.example.yummyapplication.Service;

import android.app.IntentService;
import android.content.Intent;

import com.example.yummyapplication.Model.EmployeeData;
import com.example.yummyapplication.Model.EmployeeJsonConstants;
import com.example.yummyapplication.R;
import com.example.yummyapplication.UI.IntentConstants;
import com.example.yummyapplication.Util.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Intent service that handles requests to retrieve data from json
 */
public class DataFetcherIntentService extends IntentService {
    // for future to make more generic
    private static final String FILE_URI = "com.example.yummyapplication.Service.extra.FILE_URI";

    public DataFetcherIntentService() {
        super("DataFetcherIntentService");
    }


    /**
     * Parses JSON into model objects, aggregates a set of cities, and broadcasts results in a
     * background thread
     * @param intent that is passed from activity, could be used for future generification
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String param1 = intent.getStringExtra(FILE_URI);
            try {
                ArrayList<EmployeeData> employeeDataList = handleDataRequest();
                List<CharSequence> citySet = getSetOfCities(employeeDataList);

                Intent employeeDataBroadcastIntent = new Intent();
                employeeDataBroadcastIntent.setAction(IntentConstants.EMPLOYEE_DATA_INTENT_FILTER);
                employeeDataBroadcastIntent.setPackage(getPackageName());
                employeeDataBroadcastIntent.putExtra(IntentConstants.EMPLOYEE_DATA_KEY,
                        employeeDataList);
                employeeDataBroadcastIntent.putExtra(IntentConstants.EMPLOYEE_CITIES,
                        citySet.toArray(new CharSequence[citySet.size()]));
                sendBroadcast(employeeDataBroadcastIntent);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Helper that performs the json parsing from expected json schema
     * Would have used gson, but it was misbehaving for some reason
     * @return returns the list of employees with their data
     * @throws JSONException
     */
    private ArrayList<EmployeeData> handleDataRequest() throws JSONException {
        final String jsonString =
                JsonParser.getJsonString(getResources().openRawResource(R.raw.employees));
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray employeeJsonArray = jsonObject.getJSONArray("employees");
        ArrayList<EmployeeData> employeeDataList = new ArrayList<>();

        if (employeeJsonArray != null) {
            for (int i = 0; i < employeeJsonArray.length(); i++) {
                JSONObject currentEmployeeJsonObject = employeeJsonArray.getJSONObject(i);
                if (currentEmployeeJsonObject != null) {
                    EmployeeData newEmployeeData = new EmployeeData();
                    newEmployeeData.setName(currentEmployeeJsonObject
                            .getString(EmployeeJsonConstants.NAME_KEY));
                    newEmployeeData.setTitle(currentEmployeeJsonObject
                            .getString(EmployeeJsonConstants.TITLE_KEY));
                    newEmployeeData.setLocations(
                            getLocationsFromEmployeeJsonObject(currentEmployeeJsonObject));
                    employeeDataList.add(newEmployeeData);
                }
            }
        }

        return employeeDataList;
    }

    /**
     * Helper that digs deeper into json schema for locations
     */
    private List<String> getLocationsFromEmployeeJsonObject(final JSONObject currentEmployee)
            throws JSONException {
        List<String> locations = new ArrayList<>();
        JSONArray locationsJsonArray = currentEmployee.getJSONArray(EmployeeJsonConstants.LOCATIONS_KEY);
        if (locationsJsonArray != null) {
            for (int i = 0; i < locationsJsonArray.length(); i++) {
                String currentLocation = locationsJsonArray.getString(i);
                if (currentLocation != null) {
                    locations.add(currentLocation);
                }
            }
        }
        return locations;
    }

    /**
     * Aggregates set of all cities employees have
     *
     * Could have optimized this by getting the cities while json parsing,
     * but figured this was cleaner for now
     */
    private List<CharSequence> getSetOfCities(List<EmployeeData> employeeDataList) {
        TreeSet<String> citySet = new TreeSet<>();
        for (EmployeeData employee : employeeDataList) {
            citySet.addAll(employee.getLocations());
        }

        ArrayList<CharSequence> list = new ArrayList<>();
        list.addAll(citySet);
        return list;
    }



}

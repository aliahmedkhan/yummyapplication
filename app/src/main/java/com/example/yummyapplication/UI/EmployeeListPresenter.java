package com.example.yummyapplication.UI;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yummyapplication.Model.EmployeeData;
import com.example.yummyapplication.Views.EmployeesAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class EmployeeListPresenter {
    private RecyclerView mEmployeeRV;
    private EmployeesAdapter mAdapter;
    /**
     * Can probably optimize in the presenter for more persistent storage rather
     * than loading stuff in memory, especially given that we use same info
     * redundantly
     */
    private List<EmployeeData> employeesDataSet;
    private List<EmployeeData> currentlyPresentedDataSet;

    public EmployeeListPresenter(@NonNull RecyclerView employeeRV,
                                 @NonNull WeakReference<Context> context ) {
        mEmployeeRV = employeeRV;
        mAdapter = new EmployeesAdapter();
        mEmployeeRV.setAdapter(mAdapter);
        mEmployeeRV.setLayoutManager(new GridLayoutManager(context.get(), 2));
        currentlyPresentedDataSet = new ArrayList<>(); //initially empty
    }

    /**
     * Called when total data set changes
     */
    public void onDataSetChanged(List<EmployeeData> employeeDataList) {
        employeesDataSet = employeeDataList;
        currentlyPresentedDataSet.addAll(employeesDataSet);
        mAdapter.setData(currentlyPresentedDataSet);
    }

    /**
     * Called when search query is made and presents relevant data subset
     *
     */
    public void onDialogSearchResult(String searchQuery) {
        currentlyPresentedDataSet.clear();
        for (EmployeeData employee : employeesDataSet) {
            if (containsCity(employee.getLocations(), searchQuery)) {
                currentlyPresentedDataSet.add(employee);
            }
        }

        mAdapter.setData(currentlyPresentedDataSet);
    }

    /**
     * Probably could have serialized a hashset from the service instead of a list of locations,
     * would have saved on search time. Assumption I made for v1 of the project is that the city
     * lists aren't that large
     */
    private boolean containsCity(List<String> cities, String searchCity) {
        for (String s : cities) {
            if (s.equals(searchCity)) {
                return true;
            }
        }

        return false;
    }
}

package com.example.yummyapplication.UI;

public class IntentConstants {
    public static String EMPLOYEE_DATA_INTENT_FILTER = "employee_data_intent_filter";
    public static String EMPLOYEE_DATA_KEY = "employee_data_key";
    public static String EMPLOYEE_CITIES = "employee_cities";

}

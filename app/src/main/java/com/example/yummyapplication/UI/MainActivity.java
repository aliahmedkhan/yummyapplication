package com.example.yummyapplication.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;


import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.example.yummyapplication.Model.EmployeeData;
import com.example.yummyapplication.R;
import com.example.yummyapplication.Service.DataFetcherIntentService;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {
    @BindView(R.id.shimmer_recycler_view) ShimmerRecyclerView shimmerRecyclerView;
    private EmployeeListPresenter mEmployeeListPresenter;
    private CharSequence[] mCityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Can probably optimize for activity recreation in future
        setContentView(R.layout.activity_main);
        startService(new Intent(this, DataFetcherIntentService.class));
        ButterKnife.bind(this);
        mEmployeeListPresenter = new EmployeeListPresenter(shimmerRecyclerView,
                new WeakReference<>(this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(employeeDataReceiver,
                new IntentFilter(IntentConstants.EMPLOYEE_DATA_INTENT_FILTER));
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(employeeDataReceiver);
    }

    /**
     * Shows a dialog when the floating action button is clicked that gives user cities they
     * can filter employees by
     */
    @OnClick(R.id.search_button)
    public void showSearchDialog_withCityFilters() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.search_city)
                .setItems(mCityList, (dialog, which) ->
                        mEmployeeListPresenter.onDialogSearchResult(mCityList[which].toString()));
        builder.create().show();
    }

    /**
     * Broadcast receiver that is the recipient of data results from the DataFetcherIntentService/
     * Updates presenter of dataset change and caches city list for filters user can use later
     */
    private BroadcastReceiver employeeDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction()
                    .equals(IntentConstants.EMPLOYEE_DATA_INTENT_FILTER)) {
                List<EmployeeData> employeeDataList = intent
                        .getParcelableArrayListExtra(IntentConstants.EMPLOYEE_DATA_KEY);
                mEmployeeListPresenter.onDataSetChanged(employeeDataList);
                mCityList = intent.getCharSequenceArrayExtra(IntentConstants.EMPLOYEE_CITIES);
            }
        }
    };


}

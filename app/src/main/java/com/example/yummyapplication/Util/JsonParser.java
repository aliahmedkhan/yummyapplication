package com.example.yummyapplication.Util;

import java.io.IOException;
import java.io.InputStream;

public class JsonParser {
    public static String getJsonString(InputStream is) {
        String jsonString = null;
        try {
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonString = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return jsonString;
    }
}

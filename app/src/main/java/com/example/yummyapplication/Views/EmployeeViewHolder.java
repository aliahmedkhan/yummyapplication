package com.example.yummyapplication.Views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yummyapplication.Model.EmployeeData;
import com.example.yummyapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.card_title) TextView mTitleView;
    @BindView(R.id.card_subtitle) TextView mDescView;
    @BindView(R.id.card_image) ImageView mThumbnailView;
    @BindView(R.id.card_summary) TextView mSummaryView;

    public static EmployeeViewHolder newInstance(@Nullable ViewGroup container, int type) {
        View root = LayoutInflater.from(container.getContext()).inflate(R.layout.employee_grid_item,
                container, false);

        return new EmployeeViewHolder(root);
    }

    public EmployeeViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public void bind(@NonNull EmployeeData employeeData) {
        mTitleView.setText(employeeData.getName());
        mDescView.setText(employeeData.getTitle());
        mSummaryView.setText(employeeData.getLocations().toString());
        mThumbnailView.setImageResource(getImageResourceFromTitle(employeeData.getTitle()));
    }

    private int getImageResourceFromTitle(@NonNull String title) {
        if (title.contains("Developer")) {
            return R.drawable.developer_img;
        } else if (title.contains("Designer")) {
            return R.drawable.designer_img;
        } else if (title.contains("Marketing")) {
            return R.drawable.marketing_img;
        } else {
            return R.drawable.default_img;
        }
    }

}

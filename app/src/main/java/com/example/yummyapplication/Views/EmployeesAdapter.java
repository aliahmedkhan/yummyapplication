package com.example.yummyapplication.Views;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yummyapplication.Model.EmployeeData;

import java.util.ArrayList;
import java.util.List;

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeeViewHolder> {
    private List<EmployeeData> mEmployeeList = new ArrayList<>();

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return EmployeeViewHolder.newInstance(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        if (mEmployeeList != null) {
            holder.bind(mEmployeeList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mEmployeeList != null ? mEmployeeList.size() : 0;
    }

    public void setData(@NonNull List<EmployeeData> data) {
        mEmployeeList = data;
        notifyDataSetChanged();
    }
}
